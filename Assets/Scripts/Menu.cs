﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class Menu : MonoBehaviour
{
	public AudioMixer audioMixer;

	public void jouer(){
		SceneManager.LoadScene(1);
	}

	public void retourMenu(){
		SceneManager.LoadScene(0);
	}

	public void Quitter(){
		Debug.Log("quitter");
		Application.Quit();
	}

	public void SetVolume(float volume){
		audioMixer.SetFloat("volume", volume);
	}
}
