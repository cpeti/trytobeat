﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

	public GameObject prefab1;
	public GameObject prefab2;
	public GameObject prefab3;
	public GameObject prefab4;

	private Transform parent;

	// Use this for initialization
	void Start () {
		parent = gameObject.transform;
	}
		
	public void updateMap(List<SpectralFluxInfo> pointInfo, int curIndex) {
		if ( curIndex < pointInfo.Count){
			if (pointInfo[curIndex].isPeak) {
				if (pointInfo[curIndex].spectralFlux > pointInfo[curIndex].threshold){
					//Debug.Log(pointInfo[curIndex].spectralFlux);
					int test = (int)(pointInfo[curIndex].spectralFlux*100);

					if(test%4 == 0){
						GameObject beat = Instantiate(prefab1);
						beat.transform.SetParent(parent);
					}
					if(test%4 == 1){
						GameObject beat = Instantiate(prefab2);
						beat.transform.SetParent(parent);
					}
					if(test%4 == 2){
						GameObject beat = Instantiate(prefab3);
						beat.transform.SetParent(parent);
					}
					if(test%4 == 3){
						GameObject beat = Instantiate(prefab4);
						beat.transform.SetParent(parent);
					}
					
				}
			}
		}
	}
}
