using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{

    private SpriteRenderer sr;


    public KeyCode keyToPress;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(keyToPress)){
            sr.color = new Color(0.3f, 0.3f, 0.3f, 1);
        }

        if (Input.GetKeyUp(keyToPress)){
            sr.color = new Color(1, 1, 1, 1);
        }
    }


    
}
