using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{

    public static GameManager instance = null;

    public AudioMixer audioMixer;

    public SongController songController;

    public AudioSource musique;

    public AudioSource musique2;

    public AudioSource previewMusic;

    private bool startPlaying;

    public BeatScroller bs;


    public int currentScore;
    public int scorePerNote = 50;
    public int scorePerNiceNote = 100;
    public int scorePerPerfectNote = 200;

    public int currentMultiplier;
    public int multiplierTracker;
    public int[] multiplierThresholds;

    public Text scoreText;
    public Text multiText;

    public int totalMiss;
    public int totalOk;
    public int totalNice;
    public int totalPerfect;

    public GameObject startMenu;
    public GameObject resultScreen;
    public Text percentHitText, OkText, NiceText, PerfectText, MissText, rankText, finalScoreText, titleText, lengthText;

    private int musicIndex=0;
    public AudioClip[] listMusic;



    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        scoreText.text = "Score: 0";
        multiText.text = "";
        currentScore=0;
        currentMultiplier = 1;
        SelectNormal();
        SetCurrentTitle();
    }

    // Update is called once per frame
    void Update(){
        if(startPlaying && !musique.isPlaying && !musique2.isPlaying && !resultScreen.activeInHierarchy){
                resultScreen.SetActive(true);

                OkText.text = "" + totalOk;
                NiceText.text = "" + totalNice;
                PerfectText.text = "" + totalPerfect;
                MissText.text = "" + totalMiss;
                int totalHit = totalOk+totalNice+totalPerfect;
                float percentHit = 100*totalHit/(totalHit+totalMiss);
                percentHitText.text = percentHit.ToString("F1") + "%";

                string rankVal = "D";

                if(percentHit>=50){
                    rankVal = "C";
                    if(percentHit>=70){
                        rankVal = "B";
                        if(percentHit>=90){
                            rankVal = "A";
                            if(percentHit==100){
                                rankVal = "S";
                            }
                        }
                    }
                }
                rankText.text = rankVal;

                finalScoreText.text = ""+currentScore;
        }
    }

    public void SetCurrentTitle(){
        previewMusic.Stop();
        AudioClip newClip = listMusic[musicIndex];
        musique.clip = newClip;
        musique2.clip = newClip;
        previewMusic.clip = newClip;
        titleText.text = newClip.name;
        int test  = (int)newClip.length;
        lengthText.text = test+"s";
        previewMusic.Play();

    }

    public void nextSong(){
        musicIndex++;
        if(musicIndex >= listMusic.Length){
            musicIndex = 0;
        }
        SetCurrentTitle();
    }

    public void previousSong(){
        musicIndex = musicIndex-1;
        if(musicIndex<0){
            musicIndex = listMusic.Length-1;
        }
        SetCurrentTitle();
    }

    public void StartPlay(){
        previewMusic.Stop();
        songController.InitializeMusic();
        startMenu.SetActive(false);
        startPlaying = true;
        bs.hasStarted = true;
        musique.Play();
    }

    public void SelectEasy(){
        songController.SetThresholdMultiplier(3.5f);
    }

    public void SelectNormal(){
        songController.SetThresholdMultiplier(2.3f);
    }

    public void SelectHard(){
        songController.SetThresholdMultiplier(1.8f);
    }

    public void updateText(){
        if(currentMultiplier>1){
            multiText.text = "Bonus x"+currentMultiplier;
        }
        scoreText.text = "Score: "+ currentScore;
    }

    public void NoteHit(){
        //Debug.Log("note hit");
        multiplierTracker++;
        if(currentMultiplier-1 < multiplierThresholds.Length){
            if(multiplierThresholds[currentMultiplier-1] <= multiplierTracker){
                multiplierTracker = 0;
                currentMultiplier++;
            }
        } 
        updateText();
    }

    public void NormalHit(){
        currentScore +=  scorePerNote * currentMultiplier;
        totalOk++;
        NoteHit();
        //Debug.Log("NormalHit");
    }

    public void GoodHit(){
        currentScore +=  scorePerNiceNote * currentMultiplier;
        totalNice++;
        NoteHit();
        //Debug.Log("GoodHit");
    }

    public void PerfectHit(){
        currentScore +=  scorePerPerfectNote * currentMultiplier;
        totalPerfect++;
        NoteHit();
        //Debug.Log("PerfectHit");
    }

    public void NoteMiss(){
        //Debug.Log("note miss");
        totalMiss++;
        multiplierTracker = 0;
        currentMultiplier = 1;
        multiText.text = "";
    }
}
