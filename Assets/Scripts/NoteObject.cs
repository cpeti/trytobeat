using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteObject : MonoBehaviour
{

    public bool canBePressed;

    public KeyCode keyToPress;

    public GameObject missEffect, normalEffect, goodEffect, perfectEffect;

    // Start is called before the first frame update
    void Start()
    {
        canBePressed = false;
        if(keyToPress==KeyCode.RightArrow){
            Button bouton = GameObject.Find("ButtonDroite").GetComponent<Button>();
            bouton.onClick.AddListener(() => Click());
        }
        if(keyToPress==KeyCode.LeftArrow){
            Button bouton = GameObject.Find("ButtonGauche").GetComponent<Button>();
            bouton.onClick.AddListener(() => Click());
        }
        if(keyToPress==KeyCode.DownArrow){
            Button bouton = GameObject.Find("ButtonBas").GetComponent<Button>();
            bouton.onClick.AddListener(() => Click());
        }
        if(keyToPress==KeyCode.UpArrow){
            Button bouton = GameObject.Find("ButtonHaut").GetComponent<Button>();
            bouton.onClick.AddListener(() => Click());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(keyToPress)){
            Click();
        }
    }

    public void Click(){
        if(canBePressed){
                canBePressed = false;

                if(Mathf.Abs(transform.position.y) > 0.25){
                    Instantiate(normalEffect, transform.position, normalEffect.transform.rotation);
                    GameManager.instance.NormalHit();
                }else if(Mathf.Abs(transform.position.y) > 0.1){
                    Instantiate(goodEffect, transform.position, goodEffect.transform.rotation);
                    GameManager.instance.GoodHit();
                }else{
                    Instantiate(perfectEffect, transform.position, perfectEffect.transform.rotation);
                    GameManager.instance.PerfectHit();
                }

                gameObject.AddComponent<FadeOut>();
            }   
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Activator"){
            canBePressed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.tag == "Activator"){
            if(canBePressed){
                Instantiate(missEffect, transform.position, missEffect.transform.rotation);
                GameManager.instance.NoteMiss();
            }
            canBePressed = false;

            gameObject.AddComponent<FadeOut>();
        }
    }
}
