using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatScroller : MonoBehaviour{

    public float beatTempo;

    private float speed;

    public bool hasStarted;

    //public AudioSource audioSourceMute;

    // Start is called before the first frame update
    void Start(){
        speed = beatTempo / 60f;
    }

    // Update is called once per frame
    void Update(){
        if(!hasStarted){
            /*if(Input.anyKeyDown){
                hasStarted = true;
                audioSourceMute.Play();
            }*/
        }else{
            transform.position -= new Vector3(0f, speed * Time.deltaTime*4, 0f);
        }
    }
}
