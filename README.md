# TryToBeat

Try to Beat is a rhythm game playable on pc or smartphone.
The game is still a prototype.
The game generates the levels according to the music.
The goal is to maximize your score by pressing the keys at the right time.
The game was originally designed for computer play and the smartphone controls are not yet optimal.


On computer the game is played with the help of the arrows on the keyboard, it is also possible to click on the buttons on the sides.


On smartphone you have to press the buttons on the sides to play.





Le projet à été codé sous unity 2020.2.1f1.


Le jeu analyse les musiques afin de générer les niveau, pour cette analyse je me suis basé sur les travaux présenté dans cette article et j'ai récupéré leur algorithme d'analyse.

- https://medium.com/giant-scam/algorithmic-beat-mapping-in-unity-intro-d4c2c25d2f27
- https://github.com/jesse-scam/algorithmic-beat-mapping-unity


Musiques utilisé :

- Beethoven - Für Elise
- Mozart - La marche turque
- Mozart - andante
- aiva1  composed by AIVA (Artificial Intelligence Virtual Artist): https://www.aiva.ai
